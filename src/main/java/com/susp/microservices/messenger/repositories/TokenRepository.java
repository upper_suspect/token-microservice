package com.susp.microservices.messenger.repositories;

import com.susp.microservices.messenger.entities.TokenEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface TokenRepository extends CrudRepository<TokenEntity, String> {

    @Query("SELECT entity FROM " + TokenEntity.ENTITY_NAME + " AS entity " +
            "WHERE entity." + TokenEntity.IP + " = :ipAddress " +
            "ORDER BY entity." + TokenEntity.CREATION_TIME)
    TokenEntity getTokenEntityByIp(@Param("ipAddress") String ipAddress);

    @Query("SELECT entity FROM " + TokenEntity.ENTITY_NAME + " AS entity " +
            "WHERE entity." + TokenEntity.CREATION_TIME + " <= :date " +
            "ORDER BY entity." + TokenEntity.CREATION_TIME)
    List<TokenEntity> getTokenEntityByCreationTime(@Param("date") Date date);
}
