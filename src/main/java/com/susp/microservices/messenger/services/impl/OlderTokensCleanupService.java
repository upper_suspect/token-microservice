package com.susp.microservices.messenger.services.impl;

import com.susp.microservices.messenger.entities.TokenEntity;
import com.susp.microservices.messenger.repositories.TokenRepository;
import com.susp.microservices.messenger.services.CleanupService;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service("olderTokensCleanupService")
@EnableScheduling
public class OlderTokensCleanupService implements CleanupService {

    private static final Integer DEFAULT_TIMEOUT = -5;

    @Autowired
    private TokenRepository tokenRepository;
    @Value("${cleanup.invalid.tokens.minutes.timeout}")
    private Integer timeout;
    @Value("${cleanup.invalid.tokens.enabled}")
    private Boolean enabled;

    @Override
    @Scheduled(cron = "${cleanup.invalid.tokens.cron}")
    public void cleanup() {
        if (BooleanUtils.isNotTrue(enabled)) {
            return;
        }
        Calendar calendar = Calendar.getInstance();
        if (timeout != null && timeout < 0) {
            calendar.add(Calendar.MINUTE, timeout);
        } else {
            calendar.add(Calendar.MINUTE, DEFAULT_TIMEOUT);
        }
        List<TokenEntity> olderTokens = tokenRepository.getTokenEntityByCreationTime(calendar.getTime());
        tokenRepository.delete(olderTokens);
    }
}
