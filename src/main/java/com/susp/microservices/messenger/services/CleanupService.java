package com.susp.microservices.messenger.services;

public interface CleanupService {

    void cleanup();
}
