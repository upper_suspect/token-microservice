package com.susp.microservices.messenger.services.impl;

import com.susp.microservices.messenger.entities.TokenEntity;
import com.susp.microservices.messenger.repositories.TokenRepository;
import com.susp.microservices.messenger.services.TokenService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("tokenService")
@EnableScheduling
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public TokenEntity getTokenEntity(String ip) {
        if (StringUtils.isBlank(ip)) {
            return null;
        }

        TokenEntity entity = tokenRepository.getTokenEntityByIp(ip);
        if (entity == null) {
            entity = new TokenEntity();
            entity.setToken(UUID.randomUUID());
            entity.setIp(ip);
            tokenRepository.save(entity);
            entity = tokenRepository.getTokenEntityByIp(ip);
        }

        return entity;
    }
}
