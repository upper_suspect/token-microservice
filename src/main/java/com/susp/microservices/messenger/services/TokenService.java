package com.susp.microservices.messenger.services;

import com.susp.microservices.messenger.entities.TokenEntity;

public interface TokenService {

    TokenEntity getTokenEntity(String ip);
}
