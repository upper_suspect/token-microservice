package com.susp.microservices.messenger.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name = TokenEntity.ENTITY_NAME)
@Table(name = "TOKEN_INFOS")
public class TokenEntity {

    public static final String ENTITY_NAME = "TokenInfo";

    @Getter @Setter
    @Basic(optional = false)
    @Column(name = "token", unique = true)
    private UUID token;
    public static final String TOKEN = "token";

    @Getter @Setter
    @Id
    @Basic(optional = false)
    @Column(name = "gateway_ip", nullable = false, unique = true)
    private String ip;
    public static final String IP = "ip";

    @Getter @Setter
    @Basic(optional = false)
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_time")
    private Date creationTime;
    public static final String CREATION_TIME = "creationTime";
}
