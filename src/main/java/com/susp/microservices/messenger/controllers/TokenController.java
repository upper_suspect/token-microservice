package com.susp.microservices.messenger.controllers;

import com.susp.microservices.messenger.services.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/token")
public class TokenController {

    @Autowired
    @Qualifier("tokenService")
    private TokenService tokenService;

    @RequestMapping(method = RequestMethod.GET, value = "/getToken")
    public Object getTokenEntity(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
            return tokenService.getTokenEntity(ipAddress);
        }
        return "Error! ip address not found";
    }
}
