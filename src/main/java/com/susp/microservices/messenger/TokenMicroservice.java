package com.susp.microservices.messenger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TokenMicroservice {

    public static void main(String[] args) {
        SpringApplication.run(new Class[]{TokenMicroservice.class}, args);
    }
}
